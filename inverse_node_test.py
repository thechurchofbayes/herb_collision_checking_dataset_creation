import herbpy
import os
import numpy
import math
import openravepy
import sys
import argparse
from prpy import serialization
import json
import networkx as nx


table_pose = numpy.array([[  3.29499984e-03,  -5.97027617e-08,   9.99994571e-01,
          7.83268307e-01],
       [  9.99994571e-01,  -5.95063642e-08,  -3.29499984e-03,
         -2.58088849e-03],
       [  5.97027617e-08,   1.00000000e+00,   5.95063642e-08,
          1.19378528e-07],
       [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
          1.00000000e+00]])

box_pose = numpy.array([[ 1.        ,  0.        ,  0.        ,  0.77972633],
       [ 0.        ,  1.        ,  0.        ,  0.        ],
       [ 0.        ,  0.        ,  1.        ,  0.73233283],
       [ 0.        ,  0.        ,  0.        ,  1.        ]])


def edge_to_configs(state1, state2):

    config1 = state_to_numpy(state1)
    config2 = state_to_numpy(state2)

    diff = config2 - config1
    step = diff/EDGE_DISCRETIZATION

    to_check = list()

    for i in xrange(EDGE_DISCRETIZATION - 1):
        conf = config1 + step*(i+1)
        to_check.append(conf)

    return to_check


from catkin.find_in_workspaces import find_in_workspaces

package_name = 'pr_ordata'
directory = 'data'
objects_path = find_in_workspaces(
    search_dirs=['share'],
    project=package_name,
    path=directory,
    first_match_only=True)
if len(objects_path) == 0:
    print('Can\'t find directory %s/%s' % (package_name, directory))
    sys.exit()
else:
    print objects_path # for me this is '/home/USERNAME/catkin_workspaces/herb_ws/src/pr-ordata/data/objects'
    objects_path = objects_path[0]


def state_to_numpy(state):
    strlist = state.split()
    val_list = [float(s) for s in strlist]
    return numpy.array(val_list)


def find_nearest_vtx(G,state):
    dist_vals = np.zeros((G.number_of_nodes,1))

    for (i,v) in enumerate(G.nodes()):
        v_state = state_to_numpy(G.node[v]['state'])
        dist = numpy.linalg.norm(state-v_state)
        dist_vals[i] = dist

    min_idx = numpy.argmin(dist_vals)
    return G.nodes()[min_idx]


if __name__=='__main__':

    parser = argparse.ArgumentParser(description='Generate environments')
    parser.add_argument('--graphfile',type=str,required=True)
    args = parser.parse_args()
    

    G = nx.read_graphml(args.graphfile)

    env, robot = herbpy.initialize(sim=True, attach_viewer='interactivemarker')

    table_file = os.path.join(objects_path,'objects/table.kinbody.xml')
    table = env.ReadKinBodyXMLFile(table_file)
    env.AddKinBody(table)
    table.SetTransform(table_pose)

    # box_file = os.path.join(objects_path,'objects/large_white_box.kinbody.xml')
    # box = env.ReadKinBodyXMLFile(box_file)
    # box.SetTransform(box_pose)
    # env.AddKinBody(box)

    robot.right_arm.SetActive()

    # start = numpy.array([ 5.65, -1.76, -0.26,  1.96, -1.15 , 0.87, -1.43 ])

    # start2 = state_to_numpy(G.node['9216']['state'])
    # robot.SetActiveDOFValues(start2)

    # goal = state_to_numpy(G.node['9529']['state'])

    # dist_vals = numpy.zeros((G.number_of_nodes(),1))

    # for (i,v) in enumerate(G.nodes()):
    #     v_state = state_to_numpy(G.node[v]['state'])
    #     robot.SetActiveDOFValues(v_state)
    #     if env.CheckCollision(robot) == False:
    #         dist = numpy.linalg.norm(start - v_state)
    #         dist_vals[i] = dist
    #     else:
    #         dist_vals[i] = numpy.inf

    # min_idx = numpy.argmin(dist_vals)
    from IPython import embed
    embed()


    for v in G.nodes():
      state = state_to_numpy(G.node[v]['state'])
      robot.SetActiveDOFValues(state)
      print 'For ID {0}, Collision - {1}'.format(v,env.CheckCollision(robot))
      embed()