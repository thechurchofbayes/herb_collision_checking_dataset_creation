import herbpy
import os
import numpy
import math
import openravepy
import sys
from prpy import serialization
import json

table_pose = numpy.array([[  3.29499984e-03,  -5.97027617e-08,   9.99994571e-01,
          7.83268307e-01],
       [  9.99994571e-01,  -5.95063642e-08,  -3.29499984e-03,
         -2.58088849e-03],
       [  5.97027617e-08,   1.00000000e+00,   5.95063642e-08,
          1.19378528e-07],
       [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
          1.00000000e+00]])


from catkin.find_in_workspaces import find_in_workspaces

package_name = 'pr_ordata'
directory = 'data'
objects_path = find_in_workspaces(
    search_dirs=['share'],
    project=package_name,
    path=directory,
    first_match_only=True)
if len(objects_path) == 0:
    print('Can\'t find directory %s/%s' % (package_name, directory))
    sys.exit()
else:
    print objects_path # for me this is '/home/USERNAME/catkin_workspaces/herb_ws/src/pr-ordata/data/objects'
    objects_path = objects_path[0]

env, robot = herbpy.initialize(sim=True, attach_viewer='rviz')

table_file = os.path.join(objects_path,'objects/table.kinbody.xml')
table = env.ReadKinBodyXMLFile(table_file)
env.AddKinBody(table)
table.SetTransform(table_pose)

box_file = os.path.join(objects_path,'objects/large_white_box.kinbody.xml')
box = env.ReadKinBodyXMLFile(box_file)
env.AddKinBody(box)

import IPython; IPython.embed()

z = 0.73391348
x_max = 0.88
y_min = 0.235
y_min = -0.56
x_min = 0.51

if __name__=='__main__':

    parser = argparse.ArgumentParser(description='Generate environments')