import networkx as nx
import herbpy, openravepy
import numpy
import sys
import yaml
import argparse
import math
import copy
import json
import prpy.serialization

EDGE_DISCRETIZATION = 20
NUM_FILES = 10000

ZPOS = 0.73391348
XMAX = 0.88
YMAX = 0.235
YMIN = -0.56
XMIN = 0.51


def state_to_numpy(state):
    strlist = state.split()
    val_list = [float(s) for s in strlist]
    return numpy.array(val_list)


def edge_to_configs(state1, state2):

    config1 = state_to_numpy(state1)
    config2 = state_to_numpy(state2)

    diff = config2 - config1
    step = diff/EDGE_DISCRETIZATION

    to_check = list()

    for i in xrange(EDGE_DISCRETIZATION - 1):
        conf = config1 + step*(i+1)
        to_check.append(conf)

    return to_check


if __name__=='__main__':

    # Get command line args
    parser = argparse.ArgumentParser(description='check graph edges on environment')
    parser.add_argument('--env_file',required=True)
    parser.add_argument('--graphfile',type=str,required=True)
    parser.add_argument('--output',type=str,required=True)
    args = parser.parse_args()

    G = nx.read_graphml(args.graphfile)
    output_template = args.output
    #outfile = open(args.output,'w')

    # Load up herbpy
    env,robot = herbpy.initialize(sim=True,attach_viewer='interactivemarker')
    with open(args.env_file,'r') as fin:
        data = json.load(fin)
    prpy.serialization.deserialize_environment(data,env=env,reuse_bodies = [ robot ])

    #Get box and box transform
    box = env.GetKinBody('large_white_box')
    box_pose = box.GetTransform()

    robot.right_arm.SetActive()
    fcl = openravepy.RaveCreateCollisionChecker(env, 'fcl')
    env.SetCollisionChecker(fcl)


    for n in xrange(NUM_FILES):

        print n
        outfilename = output_template+`(n+1)`+'.txt'
        outfile = open(outfilename,'w')

        #Get random x and y pos and set box pose
        xpos = XMIN + numpy.random.rand()*(XMAX - XMIN)
        ypos = YMIN + numpy.random.rand()*(YMAX - YMIN)

        print '{0} ; {1}'.format(xpos,ypos)

        box_pose[0,3] = xpos
        box_pose[1,3] = ypos

        box.SetTransform(box_pose)


        for i,edge in enumerate(G.edges()):
            u,v = edge
            state1 = G.node[u]['state']
            state2 = G.node[v]['state']
            configs_to_check = edge_to_configs(state1,state2)

            edge_free = 1

            for cc in configs_to_check:
                robot.SetActiveDOFValues(cc)

                if env.CheckCollision(robot) == True:
                    edge_free = 0
                    break

            outfile.write('{0} {1}\n'.format(i, edge_free))

        outfile.close()