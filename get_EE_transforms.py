import herbpy
import os
import numpy
import math
import openravepy
import sys
import argparse
from prpy import serialization
import json
import networkx as nx

def state_to_numpy(state):
    strlist = state.split()
    val_list = [float(s) for s in strlist]
    return numpy.array(val_list)


if __name__=='__main__':

    parser = argparse.ArgumentParser(description='Get EE transforms')
    parser.add_argument('--graphfile',type=str,required=True)
    parser.add_argument('--output',type=str,required=True)
    args = parser.parse_args()

    G = nx.read_graphml(args.graphfile)

    env,robot = herbpy.initialize(sim=True,attach_viewer='interactivemarker')

    robot.right_arm.SetActive()

    outfile = open(args.output,'w')

    for (i,v) in enumerate(G.nodes()):

        state = state_to_numpy(G.node[v]['state'])
        robot.SetActiveDOFValues(state)

        ee_trans = robot.right_arm.GetEndEffectorTransform()
        trans = ee_trans[0:3,3]
        eepos = trans.tolist()
        print '{0} : {1}'.format(i,eepos)
        outfile.write('{0} {1} {2} {3}\n'.format(v,eepos[0],eepos[1],eepos[2]))

    outfile.close()