import herbpy
import os
import numpy
import math
import openravepy
import argparse
import sys
from prpy import serialization
import json
import networkx as nx

obj_pose = numpy.array([[  3.29499984e-03,  -5.97027617e-08,   9.99994571e-01,
          7.83268307e-01],
       [  9.99994571e-01,  -5.95063642e-08,  -3.29499984e-03,
         -2.58088849e-03],
       [  5.97027617e-08,   1.00000000e+00,   5.95063642e-08,
          1.19378528e-07],
       [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
          1.00000000e+00]])


XMIN = 0.5
XMAX = 0.8
YMIN = -0.3
YMAX = 0.3
ZMIN = 0.8
ZMAX = 1.2
EDGE_DISCRETIZATION = 20
NUM_FILES = 1000

from catkin.find_in_workspaces import find_in_workspaces

package_name = 'pr_ordata'
directory = 'data'
objects_path = find_in_workspaces(
    search_dirs=['share'],
    project=package_name,
    path=directory,
    first_match_only=True)
if len(objects_path) == 0:
    print('Can\'t find directory %s/%s' % (package_name, directory))
    sys.exit()
else:
    print objects_path # for me this is '/home/USERNAME/catkin_workspaces/herb_ws/src/pr-ordata/data/objects'
    objects_path = objects_path[0]


def state_to_numpy(state):
    strlist = state.split()
    val_list = [float(s) for s in strlist]
    return numpy.array(val_list)


def edge_to_configs(state1, state2):

    config1 = state_to_numpy(state1)
    config2 = state_to_numpy(state2)

    diff = config2 - config1
    step = diff/EDGE_DISCRETIZATION

    to_check = list()

    for i in xrange(EDGE_DISCRETIZATION - 1):
        conf = config1 + step*(i+1)
        to_check.append(conf)

    return to_check


if __name__=='__main__':

    parser = argparse.ArgumentParser(description='Generate environments')
    parser.add_argument('--graphfile',type=str,required=True)
    parser.add_argument('--numfiles',type=int,required=True)
    parser.add_argument('--output',type=str,required=True)
    args = parser.parse_args()

    G = nx.read_graphml(args.graphfile)
    output_template = args.output

    env, robot = herbpy.initialize(sim=True, attach_viewer='interactivemarker')
    robot.right_arm.SetActive()
    # Load obj from pr_ordata
    obj_file = os.path.join(objects_path,'objects/large_white_box.kinbody.xml')
    obj = env.ReadKinBodyXMLFile(obj_file)
    env.AddKinBody(obj)
    numpy.random.seed()

    for n in xrange(args.numfiles):

        print n
        outfilename = output_template+`(n+1)`+'.txt'
        outfile = open(outfilename,'w')

        xpos = XMIN + numpy.random.rand()*(XMAX - XMIN)
        ypos = YMIN + numpy.random.rand()*(YMAX - YMIN)
        zpos = ZMIN + numpy.random.rand()*(ZMAX - ZMIN)

        print '{0} ; {1}'.format(xpos,ypos)

        obj_pose[0,3] = xpos
        obj_pose[1,3] = ypos
        obj_pose[2,3] = zpos

        obj.SetTransform(obj_pose)

        for i,edge in enumerate(G.edges()):
            u,v = edge
            state1 = G.node[u]['state']
            state2 = G.node[v]['state']
            configs_to_check = edge_to_configs(state1,state2)

            edge_free = 1

            for cc in configs_to_check:
                robot.SetActiveDOFValues(cc)

                if env.CheckCollision(robot) == True:
                    edge_free = 0
                    break

            outfile.write('{0} {1}\n'.format(i, edge_free))

        outfile.close()