import networkx as nx
import numpy
import argparse
from IPython import embed


def state_to_numpy(state):
	strlist = state.split()
	val_list = [float(s) for s in strlist]
	return numpy.array(val_list)




if __name__=='__main__':

	parser = argparse.ArgumentParser(description='generate graph text format from graphml')
	parser.add_argument('--graphfile',type=str,required=True)
	parser.add_argument('--output',type=str,required=True)
	args = parser.parse_args()

	G = nx.read_graphml(args.graphfile)
	outfile = open(args.output,'w')

	#print number of nodes
	numnodes = G.number_of_nodes()
	outfile.write('{0}\n'.format(numnodes))

	#print number of edges
	numedges = G.number_of_edges()
	outfile.write('{0}\n'.format(numedges))

	#for each edge, print vertices and distance
	for i,edge in enumerate(G.edges()):
		u,v = edge
		u_arr = state_to_numpy(G.node[u]['state'])
		v_arr = state_to_numpy(G.node[v]['state'])
		distance = numpy.linalg.norm(v_arr - u_arr)
		outfile.write('{0} {1} {2} {3}\n'.format(i,u,v,distance))

	outfile.close()